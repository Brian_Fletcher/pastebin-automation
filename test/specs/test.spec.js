describe('Pastebin', () => {
    it("Open Pastebin and create a paste", () => {
        browser.url('https://pastebin.com');

        // Step 1: Open pastebin.com
        expect(browser).toHaveUrl('https://pastebin.com/');

        // Step 2: Create a New Paste
        const code = 'Hello from WebDriver';
        const pasteExpiration = '10 Minutes';
        const pasteName = 'helloweb';

        $("#postform-text").setValue(code);

        $("#select2-postform-expiration-container").click();
        $(`//li[contains(text(), ${pasteExpiration})]`).click();
        $("#postform-name").setValue(pasteName);

        $("/html/body/div[1]/div[2]/div[1]/div[2]/div/form/div[5]/div[1]/div[10]/button").click();

        browser.waitUntil(
            () => browser.execute(() => document.readyState === 'complete'),
            {
                timeout: 60 * 1000, // 60 seconds
                timeoutMsg: 'Message on failure'
            }
        ).then(() => {
            let title = browser.getTitle();
            expect(title).toEqual(`${pasteName} - Pastebin.com`);
        });
    })
});